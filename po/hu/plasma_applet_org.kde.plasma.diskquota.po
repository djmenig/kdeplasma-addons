# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Kristóf Kiszel <ulysses@kubuntu.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-31 00:48+0000\n"
"PO-Revision-Date: 2019-12-03 17:54+0100\n"
"Last-Translator: Kristóf Kiszel <ulysses@kubuntu.org>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.03.70\n"

#: package/contents/ui/main.qml:77
#, fuzzy, kde-format
#| msgid "No quota restrictions found."
msgctxt "@info:status"
msgid "No quota restrictions found"
msgstr "Nincsenek kvóta korlátozások."

#: package/contents/ui/main.qml:77
#, fuzzy, kde-format
#| msgid ""
#| "Quota tool not found.\n"
#| "\n"
#| "Please install 'quota'."
msgctxt "@info:status"
msgid "Quota tool not found"
msgstr ""
"A Quota segédprogram nem található.\n"
"\n"
"Telepítse a „quota”-t."

#: package/contents/ui/main.qml:78
#, fuzzy, kde-format
#| msgid "Please install 'quota'"
msgctxt "@info:usagetip"
msgid "Please install 'quota'"
msgstr "Telepítse a „quota”-t"

#: plugin/DiskQuota.cpp:44 plugin/DiskQuota.cpp:177 plugin/DiskQuota.cpp:264
#, kde-format
msgid "Disk Quota"
msgstr "Lemezkvóta"

#: plugin/DiskQuota.cpp:45
#, kde-format
msgid "Please install 'quota'"
msgstr "Telepítse a „quota”-t"

#: plugin/DiskQuota.cpp:178
#, kde-format
msgid "Running quota failed"
msgstr "A quota futtatása meghiúsult"

#: plugin/DiskQuota.cpp:239
#, kde-format
msgctxt "usage of quota, e.g.: '/home/bla: 38% used'"
msgid "%1: %2% used"
msgstr "%1: %2% használt"

#: plugin/DiskQuota.cpp:240
#, kde-format
msgctxt "e.g.: 12 GiB of 20 GiB"
msgid "%1 of %2"
msgstr "%1 / %2"

#: plugin/DiskQuota.cpp:241
#, kde-format
msgctxt "e.g.: 8 GiB free"
msgid "%1 free"
msgstr "%1 szabad"

#: plugin/DiskQuota.cpp:261
#, kde-format
msgctxt "example: Quota: 83% used"
msgid "Quota: %1% used"
msgstr "Kvóta: %1% használt"

#: plugin/DiskQuota.cpp:265
#, kde-format
msgid "No quota restrictions found."
msgstr "Nincsenek kvóta korlátozások."
