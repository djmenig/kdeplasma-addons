# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sönke Dibbern <s_dibbern@web.de>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 02:13+0000\n"
"PO-Revision-Date: 2014-09-18 23:06+0200\n"
"Last-Translator: Sönke Dibbern <s_dibbern@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.4\n"

#: contents/config/config.qml:13
#, fuzzy, kde-format
#| msgid "General"
msgctxt "@title"
msgid "General"
msgstr "Allmeen"

#: contents/ui/main.qml:243
#, fuzzy, kde-format
#| msgid "Paste"
msgctxt "@action"
msgid "Paste"
msgstr "Infögen"

#: contents/ui/main.qml:252 contents/ui/main.qml:293
#, kde-format
msgid "Share"
msgstr "Freegeven"

#: contents/ui/main.qml:253 contents/ui/main.qml:294
#, kde-format
msgid "Drop text or an image onto me to upload it to an online service."
msgstr ""
"Hier Texten un Biller hentrecken, de Du na en Tokoppeldeenst hoochladen "
"wullt."

#: contents/ui/main.qml:294
#, kde-format
msgid "Upload %1 to an online service"
msgstr ""

#: contents/ui/main.qml:307
#, fuzzy, kde-format
#| msgid "Sending..."
msgid "Sending…"
msgstr "Bi to loosstüern..."

#: contents/ui/main.qml:308
#, kde-format
msgid "Please wait"
msgstr "Bitte töven"

#: contents/ui/main.qml:315
#, kde-format
msgid "Successfully uploaded"
msgstr "Mit Spood hoochlaadt."

#: contents/ui/main.qml:316
#, kde-format
msgid "<a href='%1'>%1</a>"
msgstr "<a href='%1'>%1</a>"

#: contents/ui/main.qml:323
#, fuzzy, kde-format
#| msgid "Error during upload. Try again."
msgid "Error during upload."
msgstr "Fehler bi't Hoochladen. Versöök dat man nochmaal."

#: contents/ui/main.qml:324
#, kde-format
msgid "Please, try again."
msgstr "Versöök dat man nochmaal"

#: contents/ui/settingsGeneral.qml:21
#, fuzzy, kde-format
#| msgid "History Size:"
msgctxt "@label:spinbox"
msgid "History size:"
msgstr "Vörgeschichtgrött:"

#: contents/ui/settingsGeneral.qml:31
#, kde-format
msgctxt "@option:check"
msgid "Copy automatically:"
msgstr ""

#: contents/ui/ShareDialog.qml:30
#, kde-format
msgid "Shares for '%1'"
msgstr ""

#: contents/ui/ShowUrlDialog.qml:42
#, kde-format
msgid "The URL was just shared"
msgstr ""

#: contents/ui/ShowUrlDialog.qml:48
#, kde-format
msgctxt "@option:check"
msgid "Don't show this dialog, copy automatically."
msgstr ""

#: contents/ui/ShowUrlDialog.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Close"
msgstr ""

#~ msgid "Text Service:"
#~ msgstr "Textdeenst:"

#~ msgid "Image Service:"
#~ msgstr "Billerdeenst:"
